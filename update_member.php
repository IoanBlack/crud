<?php

require_once 'config/connection.php';
require_once 'classes/Person.php';
require_once 'classes/Student.php';
require_once 'classes/Coach.php';
require_once 'classes/Admin.php';

if(isset($_POST['name'],$_POST['phone'],$_POST['email'],$_POST['status'],$_POST['info'],$_POST['id'])) {
    $id = $_POST['id'] ;

    switch ($_POST['status']) {
        case'student':
            $student = new Student($_POST['name'], $_POST['phone'], $_POST['email'], $_POST['status'], $_POST['info']);
            $student->update($connection,$id, 'average_mark');
            break;
        case'coach':
            $coach = new Coach($_POST['name'], $_POST['phone'], $_POST['email'], $_POST['status'], $_POST['info']);
            $coach->update($connection,$id,'subject');
            break;

        case'admin':
            $admin = new Admin($_POST['name'], $_POST['phone'], $_POST['email'], $_POST['status'], $_POST['info']);
            $admin->update($connection,$id, 'working_day');
            break;
    }
}
header('Location:index.php');