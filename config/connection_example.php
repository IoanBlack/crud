<?php
$host = '';
$dbName = '';
$dbPass = '';
$dbUser = '';

try{
    $connection = new PDO('mysql:host=' . $host . ';dbname=' . $dbName,$dbUser,$dbPass);
    $connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    $connection->exec(' SET NAMES "utf8" ');
} catch (Exception $exception){
    echo 'Error connecting to db!' . $exception->getCode() . ' msg: ' . $exception->getMessage();
    die;
}