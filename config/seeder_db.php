<?php

require_once 'connection.php';

$members = [
    [
        'full_name' => 'Sponge Bob' ,
        'role' => 'student',
        'email' => 'sponge@gmail.com',
        'phone' =>'123-213-12',
        'average_mark' => 9.1
    ],
    [
        'full_name' => 'Eric Cartman' ,
        'role' => 'student',
        'email' => 'fatboy@gmail.com',
        'phone' =>'123-213-12',
        'average_mark' => 7.3
    ],
    [
        'full_name' => 'Peter Parker' ,
        'role' => 'student',
        'email' => 'spider@gmail.com',
        'phone' =>'123-213-12',
        'average_mark' => 9.5
    ],
    [
        'full_name' => 'Bruce Wayne',
        'role' => 'coach',
        'email' => 'batman@gmail.com',
        'phone' =>'123-213-12',
        'subject' => 'justice'
    ],
    [
        'full_name' => 'Walter White' ,
        'role' => 'coach',
        'email' => 'heisenberg@gmail.com',
        'phone' =>'123-213-12',
        'subject' => 'cooking'
    ],
    [
        'full_name' => 'Tony Stark' ,
        'role' => 'coach',
        'email' => 'iron@gmail.com',
        'phone' =>'123-213-12',
        'subject' => 'trud'

    ],
    [
        'full_name' => 'Jessy Pinkmann' ,
        'role' => 'admin',
        'email' => 'chef@gmail.com',
        'phone' =>'123-213-12',
        'working_day' => 'Sunday'
    ],
    [
        'full_name' => 'Luke Skywalker' ,
        'role' => 'admin',
        'email' => 'jedi@gmail.com',
        'phone' =>'123-213-12',
        'working_day' => 'Saturday'
    ],
    [
        'full_name' => 'Jason Born' ,
        'role' => 'admin',
        'email' => 'agent@gmail.com',
        'phone' =>'123-213-12',
        'working_day' => 'Wednesday'

    ]
];

foreach ($members as $member){
try{
    switch($member['role']) {
        case'student':
            $sql = '
        INSERT INTO members SET 
        full_name = "' . $member['full_name'] . '",
        phone = "' . $member['phone'] . '",
        email = "' . $member['email'] . '", 
        role = "' . $member['role'] . '",       
        average_mark = "' . $member['average_mark'] . '" 
        ';
            break;
        case'coach':
            $sql = '
        INSERT INTO members SET 
        full_name = "' . $member['full_name'] . '",
        phone = "' . $member['phone'] . '",
        email = "' . $member['email'] . '",
        role = "' . $member['role'] . '", 
        subject = "' . $member['subject'] . '" 
        ';
            break;
        case'admin':
            $sql = '
        INSERT INTO members SET 
        full_name = "' . $member['full_name'] . '",
        phone = "' . $member['phone'] . '",
        email = "' . $member['email'] . '",  
        role = "' . $member['role'] . '" ,
        working_day = "' . $member['working_day'] . '" 
        ';
            break;
    }
            $connection->exec($sql);
} catch(Exception $exception) {
    echo 'Error inserting test data to db!' . $exception->getCode() . ' msg: ' . $exception->getMessage();
    die;
    }
}
header('Location:../index.php');