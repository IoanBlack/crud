<?php

require_once 'config/connection.php';
require_once 'classes/Person.php';
require_once 'classes/Student.php';
require_once 'classes/Coach.php';
require_once 'classes/Admin.php';

    if(isset($_GET['id'])) {
     $member = Person::getById($connection,$_GET['id'] );
    }
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Member</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Edit Member</h1>
                <form  action="update_member.php" method="post">
                    <div class="form-group">
                        <input type="hidden" name="product_id" value="<?=$member->getId()?>">
                        <label>Name:<input type="text" class="form-control" name="name" value="<?=$member->getName()?>"></label>
                        <label>Phone:<input type="text" class="form-control" name="phone" value="<?=$member->getPhone()?>"></label>
                        <label>Email:<input type="text" class="form-control" name="email" value="<?=$member->getEmail()?>"></label>
                        <label>Status <select name="status" class="form-control">
                                <option selected>Choose...</option>
                                <option value="student">student</option>
                                <option value="coach">coach</option>
                                <option value="admin">admin</option>
                            </select></label>
                        <label>Additional info:<input type="text" class="form-control" name="info"></label>
                        <input type="hidden" class="form-control" name="id" value="<?=$member->getId()?>">
                        <button class="btn btn-success form-control" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
