<?php

require_once 'config/connection.php';
require_once 'classes/Person.php';
require_once 'classes/Student.php';
require_once 'classes/Coach.php';
require_once 'classes/Admin.php';

if(isset($_POST['name'],$_POST['phone'],$_POST['email'],$_POST['status'],$_POST['info'])) {
    $status = ($_POST['status']);
    switch ($status) {
        case'student':
            $student = new Student($_POST['name'], $_POST['phone'], $_POST['email'], $_POST['status'], $_POST['info']);
            $student->store($connection,'average_mark');
            break;
        case'coach':
            $coach = new Coach($_POST['name'], $_POST['phone'], $_POST['email'], $_POST['status'], $_POST['info']);
            $coach->store($connection,'subject');;
            break;
        case'admin':
            $admin = new Admin($_POST['name'], $_POST['phone'], $_POST['email'], $_POST['status'], $_POST['info']);
            $admin->store($connection,'working_day');
            break;
    }
    header('Location:index.php');
}