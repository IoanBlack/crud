<?php

class Person
{
    protected $id;
    protected $name;
    protected $phone;
    protected $email;
    protected $role;
    protected $info;
    public function __construct($name, $phone, $email, $role,$info)

    {
        $this->name = htmlspecialchars($name);
        $this->phone = htmlspecialchars($phone);
        $this->email = htmlspecialchars($email);
        $this->role = htmlspecialchars($role);
        $this->info = htmlspecialchars($info);
    }

    public function getName()
    {
        return $this->name;
    }
    public function getPhone()
    {
        return $this->phone;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getRole()
    {
        return $this->role;
    }
    public function getInfo()
    {
        return $this->info;
    }
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    static public function getAll(PDO $connection){
        try {
            $sql = 'SELECT * FROM members';
            $pdoResult = $connection->query($sql);
            $membersArr = $pdoResult->fetchAll(PDO::FETCH_ASSOC);
            $memberObjects = [];
            foreach ($membersArr as $memberArr) {
                switch ($memberArr['role']) {
                    case'student':
                        $member = new Student($memberArr['full_name'],$memberArr['phone'],$memberArr['email'],$memberArr['role'],$memberArr['average_mark']);
                        break;
                    case'coach':
                        $member = new Coach($memberArr['full_name'],$memberArr['phone'],$memberArr['email'],$memberArr['role'],$memberArr['subject']);
                        break;
                    case'admin':
                        $member = new Admin($memberArr['full_name'],$memberArr['phone'],$memberArr['email'],$memberArr['role'],$memberArr['working_day']);
                        break;}
                        $member->setId($memberArr['id']);
                        $memberObjects[] = $member;}
            return $memberObjects;
            } catch (Exception $exception){
            echo "Error getting members! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }

    }

    static public function getById(PDO $connection,$id){
        $id = htmlspecialchars($id);
        try {
            $sql = 'SELECT * FROM members WHERE id=:id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $membersArr = $statement->fetchAll(PDO::FETCH_ASSOC);
            $memberArr = $membersArr[0];
                switch ($memberArr['role']) {
                    case'student':
                        $member = new Student($memberArr['full_name'],$memberArr['phone'],$memberArr['email'],$memberArr['role'],$memberArr['average_mark']);
                        break;
                    case'coach':
                        $member = new Coach($memberArr['full_name'],$memberArr['phone'],$memberArr['email'],$memberArr['role'],$memberArr['subject']);
                        break;
                    case'admin':
                        $member = new Admin($memberArr['full_name'],$memberArr['phone'],$memberArr['email'],$memberArr['role'],$memberArr['working_day']);
                        break;}
            $member->setId($memberArr['id']);
            return $member;
        } catch (Exception $exception){
           echo "Error getting members! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function store(PDO $connection,$indField){


        try {   $sql = "INSERT INTO members SET 
                full_name = :name,
                phone = :phone,
                email = :email,  
                role = :status,       
                $indField = :info";

            $statement = $connection->prepare($sql);
            $statement->bindValue(':name',$this->name);
            $statement->bindValue(':phone',$this->phone);
            $statement->bindValue(':email',$this->email);
            $statement->bindValue(':status',$this->role);
            $statement->bindValue(':info',$this->info);
            $statement->execute();


        } catch(Exception $exception) {
            echo 'Error storing member!' . $exception->getCode() . ' msg: ' . $exception->getMessage();
            die;
        }
    }

    public function update(PDO $connection,$id, $indField){
        try {
            $sql = "UPDATE members SET
            full_name = :name,
            phone = :phone,
            email = :email,
            role = :role,
            $indField = :info
            WHERE id = :id ";


            $statement = $connection->prepare($sql);
            $statement->execute([
                ':name' => htmlspecialchars($this->name),
                ':phone' => htmlspecialchars($this->phone),
                ':email' => htmlspecialchars($this->email),
                ':role' => htmlspecialchars($this->role),
                ':info' => htmlspecialchars($this->info),
                ':id' => htmlspecialchars($id)
            ]);

        }catch (Exception $exception){
            echo "Error updating member! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function delete($id, PDO $connection){
        try{
            $sql = 'DELETE FROM members WHERE id =:id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id',$id);
            $statement->execute();
        } catch (Exception $exception) {
            echo 'Error deleting member!';
            die;
        }
    }

    public function getVisitCard()
    {
        $ul = '<ul>';
        $ul .= '<li>' . $this->name . '</li>';
        $ul .= '<li>' . $this->role  . '</li>';
        $ul .= '<li>' . 'tel: ' . $this->phone . '</li>';
        $ul .= '<li>' . 'mail: ' . $this->email . '</li>';
        return $ul;
    }
}