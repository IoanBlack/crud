<?php

class Student extends Person
{
    public function getVisitCard()
    {
        $ul = parent::getVisitCard();
        $ul .=  '<li>' . 'Average mark: ' . $this->info . '</li>';
        $ul .= '</ul>';
        $ul .= '</div>';
        return $ul;
    }
}