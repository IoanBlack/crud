<?php

class Coach extends Person
{
    public function getVisitCard()
    {
        $ul = parent::getVisitCard();
        $ul .= '<li>' . 'subject: ' . $this->info . '</li>';
        $ul .= '</ul>';
        $ul .= '</div>';
        return $ul;
    }
}