<?php

require_once 'config/connection.php';
require_once 'classes/Person.php';
require_once 'classes/Student.php';
require_once 'classes/Coach.php';
require_once 'classes/Admin.php';

$memberObjects = Person::getAll( $connection);

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Members</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>All members</h1>
                <a href="add_member.php" class="btn btn-success">Add member</a>
                <?php if(isset($memberObjects)):?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($memberObjects as $memberObject):?>
                        <tr>
                            <td><?=$memberObject->getId()?></td>
                            <td><?=$memberObject->getName()?></td>
                            <td><?=$memberObject->getRole()?></td>
                            <td>
                                <a href="show_member.php?id=<?=$memberObject->getId()?>" class="btn btn-info">Show</a>
                                <a href="edit_member.php?id=<?=$memberObject->getId()?>" class="btn btn-warning">Edit</a>
                                <a onclick="return confirm('are you sure?')" href="delete_member.php?id=<?=$memberObject->getId()?>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</body>
</html>