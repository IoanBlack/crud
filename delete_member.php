<?php

require_once 'config/connection.php';
require_once 'classes/Person.php';

if(isset($_GET['id'])) {
    $id = $_GET['id'];
  Person::delete($id,$connection);
    header('Location:index.php');
}