<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Member</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Add Member</h1>
                <form  action="store_member.php" method="post">
                    <div class="form-group">
                        <label>Name:<input type="text" class="form-control" name="name"></label>
                        <label>Phone:<input type="text" class="form-control" name="phone"></label>
                        <label>Email:<input type="text" class="form-control" name="email"></label>
                        <label>Status <select name="status" class="form-control">
                                <option selected>Choose...</option>
                                <option value="student">student</option>
                                <option value="coach">coach</option>
                                <option value="admin">admin</option>
                            </select></label>
                        <label>Additional info:<input type="text" class="form-control" name="info"></label>
                        <button class="btn btn-success form-control" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
